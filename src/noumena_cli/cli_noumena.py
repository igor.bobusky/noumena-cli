import click
import os
import pandas as pd
import sys
import io

path = os.path.dirname(os.path.abspath(__file__))


@click.command()
@click.argument('output_format')
@click.option(
    '--input-format', '-if',
    help='Specify input formats. Possible formats are csv and prn',
    default='csv',
    show_default=True
)
def main(output_format, input_format):
    """
    Command line utility which will read these CSV files and PRN files from stdin and,
    based on a command line option, print either JSON or HTML to stdout, so that it would work as part of a
    command pipeline.
    """

    file_encoding = 'latin1'
    input_bytes = sys.stdin.buffer.read()
    input_decoded = input_bytes.decode("latin1")

    try:

        if input_format == 'prn':
            data = pd.DataFrame(
                pd.read_fwf(
                    io.StringIO(input_decoded),
                    header=0,
                    encoding=file_encoding
                )
            )
        else:
            csv = pd.read_csv(
                io.StringIO(input_decoded),
                sep=",",
                header=0,
                index_col=False,
                encoding=file_encoding
            )
            data = pd.DataFrame(csv)

        data['Birthday'] = pd.to_datetime(data['Birthday']).dt.strftime('%m/%d/%Y')
        output( output_format, data)
    except Exception as e:
        print("Error %s" % e)


def output(output_format, output_data_frame):
    if output_format == 'json':
        print(output_data_frame.to_json())
    else:
        print(output_data_frame.to_html())


if __name__ == '__main__':
    main()
