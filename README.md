# noumena-cli - Data format translation program

## Description
Command line utility which will read CSV files and PRN files from stdin and, 
based on a command line option, print either JSON or HTML to stdout, so that it would work as part of a 
command pipeline.

Identical input content (i.e the data in the file, minus the formatting characters) 
should produce identical output - irrespective of whether the input data format was CSV or PRN. 
Non ASCII characters should be handled and rendered correctly. 
No content should be lost in translation and all output should be readable when encoded to UTF-8.


## How to install and run
- `python3 -m venv ml_env && source ml_env/bin/activate`
- `pip3 install -r requirements.txt`
- `python3 src/noumena_cli/cli_noumena.py json --input-format csv < src/noumena_cli/Workbook2.csv`
- `python3 src/noumena_cli/cli_noumena.py json --input-format prn < src/noumena_cli/Workbook2.prn`

## How to release
python -m pip install --upgrade pip setuptools wheel
python -m pip install --user --upgrade twine
python setup.py bdist_wheel
python -m pip install dist/noumena_cli-1.0.0-py3-none-any.whl
