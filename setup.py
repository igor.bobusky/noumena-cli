"""A setuptools based setup module.
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import
from io import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='noumena-cli',  # Required
    version='1.0.0',  # Required
    description='Data format translation program',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)
    url='https://gitlab.com/igor.bobusky/noumena-cli',  # Optional
    author='Igor Bobusky',  # Optional
    author_email='igor.bobusky@gmail.com',  # Optional
    classifiers=[  # Optional
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    keywords='noumena cli',  # Optional
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),  # Required
    python_requires='>=3.5',
    install_requires=[
        'click'
    ],  # Optional
    extras_require={  # Optional
        # 'dev': ['check-manifest'],
        # 'test': ['coverage'],
    },
    package_data={  # Optional
        # 'sample': ['package_data.dat'],
    },
    data_files=[
        # ('my_data', ['data/data_file'])
    ],  # Optional
    entry_points={  # Optional
        'console_scripts': [
            'noumena_cli=noumena_cli.cli_noumena:main',
        ],
    }
)